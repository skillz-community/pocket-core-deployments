#!/usr/bin/expect

set timeout -1

while { true } {
  spawn sh -c "wget http://localhost:26657/status -O- | grep catching"
  expect eof
  set sync_status $expect_out(buffer)
  if {[string match "*false*" $sync_status]} {
     break;
  }
  sleep 3
}


spawn sh -c "pocket accounts list | head -n 1 | cut -d ' ' -f 2 | tr -d '\n'"
expect eof
set pocket_node_address $expect_out(buffer)
spawn sh -c "pocket query node $pocket_node_address"
expect eof
set pocket_node_status $expect_out(buffer)
if {[string match "*validator not found*" $pocket_node_status]} {
  set pocket_stake_output "*\"code\":*"
  while {[string match "*\"code\":*" $pocket_stake_output]} {
    puts "Unjailing in progress"
    spawn pocket nodes stake $pocket_node_address 15005000000 0001,0021 https://$env(POCKET_CORE_URL):443 mainnet 10000 --remoteCLIURL http://pocket-fullnode.nodes.ba2s.skillz.io:8081
    expect "Enter"
    send -- "$env(POCKET_CORE_PASSPHRASE)\n"
    expect eof
    set pocket_stake_output $expect_out(buffer)
    sleep 180
  }
  puts "Done staking."
  exit 0
}

spawn sh -c "pocket query node $pocket_node_address | tail +2 | jq '.jailed' | tr -d '\n'"
expect eof
set pocket_node_status $expect_out(buffer)
if {[string match "true" $pocket_node_status]} {
  set pocket_unjail_output "*\"code\":*"
  while {[string match "*\"code\":*" $pocket_unjail_output]} {
    spawn sh -c "pocket nodes unjail $pocket_node_address mainnet 10000 --remoteCLIURL http://pocket-fullnode.nodes.ba2s.skillz.io:8081"
    expect "Enter"
    send -- "$env(POCKET_CORE_PASSPHRASE)\n"
    expect eof
    set pocket_unjail_output $expect_out(buffer)
    sleep 180
  }
  puts "Done unjail."
  exit 0
}
