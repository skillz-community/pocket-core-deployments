#!/usr/bin/expect
set command $argv; # Grab the first command line parameter
set timeout -1
spawn sh -c "cp /tmp/*.json /home/app/.pocket/config/"
sleep 2
if { [info exists ::env(POCKET_CORE_USE_ARMORED)] } {
        spawn pocket accounts import-armored $env(POCKET_CORE_KEY_FILE)
        expect "decrypt"
        send -- "$env(POCKET_CORE_PASSPHRASE)\n"
        expect "encrypt"
        send -- "$env(POCKET_CORE_PASSPHRASE)\n"
        expect eof
        spawn sh -c "pocket accounts set-validator `pocket accounts list | cut -d' ' -f2- `"
        sleep 1
        send -- "$env(POCKET_CORE_PASSPHRASE)\n"
        expect eof
        spawn sh -c "$command"
        expect eof
        exit
}
if { ![info exists ::env(POCKET_CORE_KEY)] }  {
    spawn sh -c "$command"
    send -- "$env(POCKET_CORE_PASSPHRASE)\n"
} else {
    spawn pocket accounts import-raw $env(POCKET_CORE_KEY)
    sleep 1
    send -- "$env(POCKET_CORE_PASSPHRASE)\n"
    expect eof
    spawn sh -c "pocket accounts set-validator `pocket accounts list | cut -d' ' -f2- `"
    sleep 1
    send -- "$env(POCKET_CORE_PASSPHRASE)\n"
    expect eof
    spawn sh -c "$command"
}
expect eof
exit
