#!/usr/bin/env sh

READINESS_PKT_HEIGHT_1=$(pocket query height | tail +2 | jq '.height' | tr -d '\n')
READINESS_PKT_HEIGHT_1=$(($READINESS_PKT_HEIGHT_1 + 0))
sleep 5
READINESS_PKT_HEIGHT_2=$(pocket query height | tail +2 | jq '.height' | tr -d '\n')
READINESS_PKT_HEIGHT_2=$(($READINESS_PKT_HEIGHT_2 + 0))
if [ "$READINESS_PKT_HEIGHT_1" != "$READINESS_PKT_HEIGHT_2" ] && [ $READINESS_PKT_HEIGHT_1 -ne $(($READINESS_PKT_HEIGHT_2 - 1)) ]; then
    exit 1
fi
exit 0